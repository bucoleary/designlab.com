---
name: Select
description: An HTML element, typically used in a form.
componentLabel: form-select
---

## Examples

<story-viewer component="base-form-form-select" title="Select"></story-viewer>

[View in Pajamas UI Kit →](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/%F0%9F%93%99-Component-library?type=design&node-id=49840-75722&mode=design)

## Structure

<todo>Add structure image.</todo>

## Guidelines

### When to use

- To indicate a selection of a single item from a list of options.
- To present sort options when the sort order button isn't required (see the [sorting](/components/sorting) component for a comparison).

### When not to use

- If there are five or less options to select from or the user needs to easily see all of options, then consider using [radio buttons](/components/radio-button) instead.
- If more than one option can be selected, consider using [checkboxes](/components/checkbox) instead.
- If the option is a binary decision that takes immediate effect, consider using a [toggle](/components/toggle) instead.
- If selecting from a list of options outside of a [form](/patterns/forms) or for more complex interactions, then a combobox is preferred.

### Appearance

<todo>Add appearance.</todo>

### Behavior

<todo>Add behavior.</todo>

### Accessibility

<todo>Add accessibility.</todo>
