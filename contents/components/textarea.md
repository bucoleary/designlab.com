---
name: Textarea
description: A component for the HTML textarea element.
componentLabel: form-textarea
---

## Examples

<story-viewer component="base-form-form-textarea" title="Textarea"></story-viewer>

[View in Pajamas UI Kit →](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/%F0%9F%93%99-Component-library?type=design&node-id=49840-75722&mode=design)

## Structure

<todo>Add structure image.</todo>

## Guidelines

<todo>Add guidelines.</todo>

### Appearance

<todo>Add appearance.</todo>

### Behavior

<todo>Add behavior.</todo>

### Accessibility

<todo>Add accessibility.</todo>
